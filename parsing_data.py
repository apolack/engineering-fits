import csv

# Assuming your text file has values separated by some delimiter, e.g., tab or comma
input_file_path = 'fits_raw_data.txt'
output_file_path = 'output.csv'

# Open the text file for reading
with open(input_file_path, 'r') as text_file:
    # Read lines from the text file
    lines = text_file.readlines()

# Process the lines and extract data (replace this with your logic)
data = []
for line in lines:
    # Assuming values are separated by tabs, modify as needed
    values = line.strip().split(' ')
    data.append(values)

# Open the CSV file for writing
with open(output_file_path, 'w', newline='') as csv_file:
    # Create a CSV writer object
    csv_writer = csv.writer(csv_file)

    # Write data to the CSV file
    csv_writer.writerows(data)

print(f'Text file "{input_file_path}" has been successfully parsed and saved as CSV file "{output_file_path}".')
