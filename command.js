const tolerances = {
    LOOSE_RUNNING: { hb_hole: 'H11', hb_shaft: 'c11', sb_hole: 'C11', sb_shaft: 'h11' },
    FREE_RUNNING: { hb_hole: 'H9', hb_shaft: 'd9', sb_hole: 'H9', sb_shaft: 'd9' },
    CLOSE_RUNNING: { hb_hole: 'H8', hb_shaft: 'f7', sb_hole: 'F8', sb_shaft: 'h7' },
    SLIDING: { hb_hole: 'H7', hb_shaft: 'g6', sb_hole: 'G7', sb_shaft: 'h6' },
    LOCATION: { hb_hole: 'H7', hb_shaft: 'h6', sb_hole: 'H7', sb_shaft: 'h6' },
    SIMILAR_FIT: { hb_hole: 'H7', hb_shaft: 'k6', sb_hole: 'K7', sb_shaft: 'h6' },
    FIXED_FIT: { hb_hole: 'H7', hb_shaft: 'n6', sb_hole: 'N7', sb_shaft: 'h6' },
    PRESS_FIT: { hb_hole: 'H7', hb_shaft: 'p6', sb_hole: 'P7', sb_shaft: 'h6' },
    DRIVING_FIT: { hb_hole: 'H7', hb_shaft: 's6', sb_hole: 'S7', sb_shaft: 'h6' },
    FORCED_FIT: { hb_hole: 'H7', hb_shaft: 'u6', sb_hole: 'U7', sb_shaft: 'h6' },
    // Add more colors as needed
};

document.addEventListener('DOMContentLoaded', function () {
    // Add event listeners to the checkboxes
    var shaftCheckbox = document.getElementById('shaft');
    var holeCheckbox = document.getElementById('hole');
    var shaftLabel = document.getElementById('shaftLabel');
    var holeLabel = document.getElementById('holeLabel');

    shaftCheckbox.addEventListener('change', function () {
        toggleShaftInput();

        // If shaft checkbox is checked, hide hole checkbox and label
        holeCheckbox.style.display = shaftCheckbox.checked ? 'none' : 'block';
        holeLabel.style.display = shaftCheckbox.checked ? 'none' : 'block';
    });

    holeCheckbox.addEventListener('change', function () {
        toggleHoleInput();

        // If hole checkbox is checked, hide shaft checkbox and label
        shaftCheckbox.style.display = holeCheckbox.checked ? 'none' : 'block';
        shaftLabel.style.display = holeCheckbox.checked ? 'none' : 'block';
    });
});

function toggleShaftInput() {
    // Get the checkbox and subscribe input elements
    var shaftCheckbox = document.getElementById('shaft');
    var shaftInputContainer_min = document.getElementById('shaftInputContainer_min');
    var shaftInputContainer_max = document.getElementById('shaftInputContainer_max');

    // Toggle the display of the subscribe input based on the checkbox state
    shaftInputContainer_min.style.display = shaftCheckbox.checked ? 'block' : 'none';
    shaftInputContainer_max.style.display = shaftCheckbox.checked ? 'block' : 'none';
}

function toggleHoleInput() {
    // Get the checkbox and subscribe input elements
    var holeCheckbox = document.getElementById('hole');
    var holeInputContainer_min = document.getElementById('holeInputContainer_min');
    var holeInputContainer_max = document.getElementById('holeInputContainer_max');

    // Toggle the display of the subscribe input based on the checkbox state
    holeInputContainer_min.style.display = holeCheckbox.checked ? 'block' : 'none';
    holeInputContainer_max.style.display = holeCheckbox.checked ? 'block' : 'none';
}

function collectAndDisplay() {
    
    // Collect user inputs
    var dim = parseFloat(document.getElementById('dim').value);
    var shaftCheckbox = document.getElementById('shaft');
    var shaftInput_min = parseFloat(shaftCheckbox.checked ? document.getElementById('shaftInput_min').value : 'N/A' );
    var shaftInput_max = parseFloat(shaftCheckbox.checked ? document.getElementById('shaftInput_max').value : 'N/A' );
    var holeCheckbox = document.getElementById('hole');
    var holeInput_min = parseFloat(holeCheckbox.checked ? document.getElementById('holeInput_min').value : 'N/A' );
    var holeInput_max = parseFloat(holeCheckbox.checked ? document.getElementById('holeInput_max').value : 'N/A' );
    var fits = document.getElementById('fits').value;
    var dfm = document.getElementById('dfm').value;
    // Store the collected data
    storeUserData(dim, shaftCheckbox.checked, shaftInput_min, shaftInput_max, holeCheckbox.checked, holeInput_min, holeInput_max, fits, dfm);

    // Display the collected data
    displayUserData(dim, shaftCheckbox.checked, shaftInput_min, shaftInput_max, holeCheckbox.checked, holeInput_min, holeInput_max, fits, dfm);
}

function storeUserData(dim, OTSshaft, shaftInput_min, shaftInput_max, OTShole, holeInput_min, holeInput_max, fits, dfm) {
    // Store the data as needed, e.g., in an array, object, or send it to a server
    var userData = {
        dim: dim,
        OTSshaft: OTSshaft,
        shaftInput_min: OTSshaft ? shaftInput_min : 'N/A',
        shaftInput_max: OTSshaft ? shaftInput_max : 'N/A',
        OTShole: OTShole,
        holeInput_min: OTShole ? holeInput_min : 'N/A',
        holeInput_max: OTShole ? holeInput_max : 'N/A',
        fits: fits,
        dfm: dfm
    };

    const toleranceObject = tolerances[fits];

    if(OTShole == true){
        var holesize = toleranceObject.hb_hole;
        var shaftsize = toleranceObject.hb_shaft;
        parseCSV1(shaftsize, holesize, dim, userData);
       // newRange(holeInput_min, holeInput_max, shaftInput_min, shaftInput_max, OTShole, OTSshaft, dim);
    } else if(OTSshaft == true){
        var holesize = toleranceObject.sb_hole;
        var shaftsize = toleranceObject.sb_shaft;
        parseCSV1(shaftsize, holesize, dim, userData);
       // newRange(holeInput_min, holeInput_max, shaftInput_min, shaftInput_max, OTShole, OTSshaft, dim);
    }
    console.log('Stored User Data:', userData);
}

async function parseCSV1(shaftsize, holesize, dim, userData){
    // Fetch the CSV file
    fetch('hole_data.csv')
    .then(response => response.text())
    .then(csvData => {
        // Process the CSV data into an array of objects
        const rows = csvData.trim().split('\n');
        const header = rows[0].split(',').map(item => item.trim());
        const holedata = rows.slice(1).map(row => {
            const values = row.split(',').map(item => item.trim());
            const rowData = {};
            header.forEach((key, index) => {
                rowData[key] = values[index];
            });
            return rowData;
        });

        for (let i = 0; i < holedata.length; i++) {
            for (const prop in holedata[i]) {
                if (Object.prototype.hasOwnProperty.call(holedata[i], prop)) {
                    // Check if the value is a string and can be converted to a float
                    if (typeof holedata[i][prop] === 'string' && !isNaN(parseFloat(holedata[i][prop]))) {
                        // Convert the string value to a float
                        holedata[i][prop] = parseFloat(holedata[i][prop]);
                    }
                }
            }
        }
        // Log the processed data
        console.log('Hole Data:', holedata);

        parseCSV(shaftsize, holesize, dim, userData, holedata)

        // Example: Search for a specific value
        //const searchResult = data.find(item => item.Name === 'John');
        //console.log('Search Result:', searchResult);
    })
    .catch(error => console.error('Error fetching CSV:', error));

}

async function parseCSV(shaftsize, holesize, dim, userData, holedata){
    // Fetch the CSV file
    fetch('shaft_data.csv')
    .then(response => response.text())
    .then(csvData => {
        // Process the CSV data into an array of objects
        const rows = csvData.trim().split('\n');
        const header = rows[0].split(',').map(item => item.trim());
        const shaftdata = rows.slice(1).map(row => {
            const values = row.split(',').map(item => item.trim());
            const rowData = {};
            header.forEach((key, index) => {
                rowData[key] = values[index];
            });
            return rowData;
        });

        for (let i = 0; i < shaftdata.length; i++) {
            for (const prop in shaftdata[i]) {
                if (Object.prototype.hasOwnProperty.call(shaftdata[i], prop)) {
                    // Check if the value is a string and can be converted to a float
                    if (typeof shaftdata[i][prop] === 'string' && !isNaN(parseFloat(shaftdata[i][prop]))) {
                        // Convert the string value to a float
                        shaftdata[i][prop] = parseFloat(shaftdata[i][prop]);
                    }
                }
            }
        }

        calculateTolerance(shaftsize, holesize, dim, userData, shaftdata, holedata);

        // Log the processed data
        console.log('Shaft Data:', shaftdata);

        // Example: Search for a specific value
        //const searchResult = data.find(item => item.Name === 'John');
        //console.log('Search Result:', searchResult);
    })
    .catch(error => console.error('Error fetching CSV:', error));

}

function calculateTolerance(shaftsize, holesize, dim, userData, shaftdata, holedata){
    // Log the processed data
    console.log('shaftsize: ', shaftsize);
    console.log('holesize: ', holesize);
    console.log('dim: ', dim);
    
    var shaft_min_range = 0;
    var shaft_max_range = 0;    
    var shaft_range = 0;

    var hole_min_range = 0;
    var hole_max_range = 0;    
    var hole_range = 0;

    //shaft tolerances
    for (let i = 0; i < shaftdata.length; i++) {
        if (dim > shaftdata[i].range && dim <= shaftdata[i+1].range){

            console.log('shaftdata[i].range: ', shaftdata[i].range);
            console.log('shaftdata[i+1].range: ', shaftdata[i+1].range);

            const bottomshaftrange = parseFloat(shaftdata[i+1][shaftsize])
            const topshaftrange = parseFloat(shaftdata[i][shaftsize])

            console.log('bottomshaftrange: ', bottomshaftrange);
            console.log('topshaftrange: ', topshaftrange);

            shaft_min_range = dim + bottomshaftrange;
            shaft_max_range = dim + topshaftrange;

            shaft_range = parseFloat((shaft_max_range - shaft_min_range).toFixed(3));
            break;
        }
    }

    console.log('shaft_min_range: ', shaft_min_range);
    console.log('shaft_max_range: ', shaft_max_range);
    console.log('shaft_range: ', shaft_range);

    //hole tolerances
    for (let i = 0; i < holedata.length; i++) {
        if (dim > holedata[i].range && dim <= holedata[i+1].range){

            console.log('holedata[i].range: ', holedata[i].range);
            console.log('holedata[i+1].range: ', holedata[i+1].range);

            const bottomholerange = parseFloat(holedata[i+1][holesize])
            const topholerange = parseFloat(holedata[i][holesize])

            console.log('bottomholerange: ', bottomholerange);
            console.log('topholerange: ', topholerange);

            hole_min_range = dim + bottomholerange;
            hole_max_range = dim + topholerange;

            hole_range = parseFloat((hole_max_range - hole_min_range).toFixed(3));
            break;
        }
    }

    console.log('hole_min_range: ', hole_min_range);
    console.log('hole_max_range: ', hole_max_range);
    console.log('hole_range: ', hole_range);

    var clear = parseFloat((hole_max_range - shaft_min_range).toFixed(3));
    var int = int = parseFloat((shaft_max_range - hole_min_range).toFixed(3));
    console.log('clear: ', clear);
    console.log('int: ', int);

    //calculate the max and min dimensions of the OTS part
    if(userData.OTShole === true){
        let max = parseFloat((userData.holeInput_max).toFixed(3));
        let min = parseFloat((userData.holeInput_min).toFixed(3));
        OTSmax = dim + max;
        OTSmin = dim + min;
    }else if(userData.OTSshaft === true){
        let max = parseFloat((userData.shaftInput_max).toFixed(3));
        let min = parseFloat((userData.shaftInput_min).toFixed(3));
        OTSmax = dim + max;
        OTSmin = dim + min;
    }

    var min_search_range;
    var max_search_range;

    if(userData.OTShole == true){
        min_search_range = ((OTSmax - clear) - dim).toFixed(3);
        max_search_range = ((OTSmin + int) - dim).toFixed(3);
    }else if(userData.OTSshaft == true){
        min_search_range = ((OTSmax - int) - dim).toFixed(3);
        max_search_range = ((OTSmin + clear) - dim).toFixed(3);
    }

    console.log('min search: ', min_search_range);
    console.log('max search: ', max_search_range);

    displayIdealRange(max_search_range, min_search_range);

    var contained_range;
    var tight_range;
    var loose_range;

    if(userData.OTShole){
        contained_range = containedRange(min_search_range, max_search_range, dim, shaftdata, userData);
        tight_range = tightRange(min_search_range, max_search_range, dim, shaftdata, userData);
        loose_range = looseRange(min_search_range, max_search_range, dim, shaftdata, userData);
    }else if(userData.OTSshaft){
        contained_range = containedRange(min_search_range, max_search_range, dim, holedata, userData);
        tight_range = tightRange(min_search_range, max_search_range, dim, holedata, userData);
        loose_range = looseRange(min_search_range, max_search_range, dim, holedata, userData);
    }

    console.log('contained range: ', contained_range);
    console.log('tight range: ', tight_range);
    console.log('loose range: ', loose_range);
}

function containedRange(newmin, newmax, dim, data, userData){
    var newTol;
    var minTol;
    var maxTol;
    var idx1;
    var idx2;
    const searchType = "Contained";

    for (let i = 0; i < data.length; i++) {
        if (dim > data[i].range && dim <= data[i+1].range){
            idx2 = data[i+1];
            idx1 = data[i];
            break;
        }
    }

    console.log('idx1:', idx1);
    console.log('idx2:', idx2);

    // Assuming data is an array of objects representing rows from your CSV
   // const rowsToCompare = [idx1, idx2]; // Assuming you want to compare rows with indices 2 and 5

    var maxbuffer = 100;
    var minbuffer = 100;
    var absbuffer = 100;
    var oldabsbuffer = 100;

    var myNumber = parseFloat(userData.dfm);

    for (const prop of Object.keys(idx1).slice(2)) {
        const narrowed_search_max = idx1[prop];
        const narrowed_seach_min = idx2[prop];
    
        // Your code here
        console.log(prop, narrowed_search_max, narrowed_seach_min);

        // Your criteria for comparison
        if (narrowed_seach_min >= newmin && narrowed_search_max <= newmax) {
            var tempminbuffer = parseFloat((Math.abs(newmin - narrowed_seach_min)).toFixed(3));
            var tempmaxbuffer = parseFloat((Math.abs(newmax - narrowed_search_max)).toFixed(3));
            //console.log('temp min buffer:', tempminbuffer);
            // console.log('temp max buffer:', tempmaxbuffer);

            absbuffer = (((tempmaxbuffer*1000) + (tempminbuffer*1000))/1000).toFixed(3);
            //console.log('temp abs buffer:', absbuffer);

            if(absbuffer < oldabsbuffer ){
                var goodDFM = checkDFM(prop, myNumber);
                if(goodDFM == true){
                    oldabsbuffer = absbuffer;
                    maxbuffer = tempmaxbuffer;
                    minbuffer = tempminbuffer;
                    newTol = prop; 
                    minTol = narrowed_seach_min;
                    maxTol = narrowed_search_max;
                }
            }
        }
    }

    //gonna need to add stuff here about min DFM allowences but it proablu wont even work
    console.log('newTol CONTAINED:', newTol);
    console.log('min buffer:', minbuffer);
    console.log('max buffer:', maxbuffer);
    console.log('abs buffer:', absbuffer);
    console.log('min tol:', minTol);
    console.log('max tol:', maxTol);

    displayContainedOutput(minbuffer, maxbuffer, newTol, minTol, maxTol, userData, searchType); 
    
    return newTol;
}

function tightRange(newmin, newmax, dim, data, userData){
    var newTol;
    var minTol;
    var maxTol;
    var idx1;
    var idx2;
    const searchType = "Tight";
    

    for (let i = 0; i < data.length; i++) {
        if (dim > data[i].range && dim <= data[i+1].range){
            idx2 = data[i+1];
            idx1 = data[i];
            break;
        }
    }

    // Assuming data is an array of objects representing rows from your CSV
   // const rowsToCompare = [idx1, idx2]; // Assuming you want to compare rows with indices 2 and 5

    var maxbuffer = 100;
    var minbuffer = 100;
    var absbuffer = 100;
    var oldabsbuffer = 100;

    var myNumber = parseFloat(userData.dfm);

    for (const prop of Object.keys(idx1).slice(2)) {
        const narrowed_search_max = idx1[prop];
        const narrowed_seach_min = idx2[prop];
    
        // Your code here
        //console.log(prop, narrowed_search_max, narrowed_seach_min);

        // Your criteria for comparison
        if (narrowed_seach_min >= newmin && narrowed_search_max >= newmax) {
            var tempminbuffer = parseFloat((Math.abs(newmin - narrowed_seach_min)).toFixed(3));
            var tempmaxbuffer = parseFloat((Math.abs(newmax - narrowed_search_max)).toFixed(3));
            absbuffer = tempmaxbuffer + tempminbuffer;

            if(absbuffer < oldabsbuffer ){
                var goodDFM = checkDFM(prop, myNumber);
                if(goodDFM == true){
                    oldabsbuffer = absbuffer;
                        maxbuffer = tempmaxbuffer;
                        minbuffer = tempminbuffer;
                        newTol = prop; 
                        minTol = narrowed_seach_min;
                        maxTol = narrowed_search_max;
                    }
            }
        }
       
    }

    //gonna need to add stuff here about min DFM allowences but it proablu wont even work
    console.log('newTol TIGHT:', newTol);
    console.log('min buffer:', minbuffer);
    console.log('max buffer:', maxbuffer);
    console.log('abs buffer:', absbuffer);
    
    displayTightOutput(minbuffer, maxbuffer, newTol, minTol, maxTol, userData, searchType);

    return newTol;
}

function looseRange(newmin, newmax, dim, data, userData){
    var newTol;
    var minTol;
    var maxTol;
    var idx1;
    var idx2;
    const searchType = "Loose";

    for (let i = 0; i < data.length; i++) {
        if (dim > data[i].range && dim <= data[i+1].range){
            idx2 = data[i+1];
            idx1 = data[i];
            break;
        }
    }

    // Assuming data is an array of objects representing rows from your CSV
   // const rowsToCompare = [idx1, idx2]; // Assuming you want to compare rows with indices 2 and 5

    var maxbuffer = 100;
    var minbuffer = 100;
    var absbuffer = 100;
    var oldabsbuffer = 100;

    var myNumber = parseFloat(userData.dfm);

    for (const prop of Object.keys(idx1).slice(2)) {
        const narrowed_search_max = idx1[prop];
        const narrowed_seach_min = idx2[prop];
    
        // Your code here
        //console.log(prop, narrowed_search_max, narrowed_seach_min);

        // Your criteria for comparison
       
        if (narrowed_seach_min <= newmin && narrowed_search_max <= newmax) {
            var tempminbuffer = parseFloat((Math.abs(newmin - narrowed_seach_min)).toFixed(3));
            var tempmaxbuffer = parseFloat((Math.abs(newmax - narrowed_search_max)).toFixed(3));
            absbuffer = tempmaxbuffer + tempminbuffer;

            if(absbuffer < oldabsbuffer ){
                var goodDFM = checkDFM(prop, myNumber);
                if(goodDFM == true){
                    oldabsbuffer = absbuffer;
                        maxbuffer = tempmaxbuffer;
                        minbuffer = tempminbuffer;
                        newTol = prop; 
                        minTol = narrowed_seach_min;
                        maxTol = narrowed_search_max;
                    }
            }
        }
    }

    //gonna need to add stuff here about min DFM allowences but it proablu wont even work
    console.log('newTol LOOSE:', newTol);
    console.log('min buffer:', minbuffer);
    console.log('max buffer:', maxbuffer);
    console.log('abs buffer:', absbuffer);
    
    displayLooseOutput(minbuffer, maxbuffer, newTol, minTol, maxTol, userData, searchType);
    
    return newTol;
}

function displayContainedOutput(minbuffer, maxbuffer, newTol, minTol, maxTol, userData, searchType) {
    // Access the display area in HTML
    var finalContainedOutput = document.getElementById('finalContainedOutput');

    if(userData.OTShole == true && minbuffer < 100){
        var content = `
        <h2>${searchType} Output</h2>
        <p><strong>Suggested Shaft Tolerance:</strong> ${newTol}</p>
        <p><strong>Tolerance Range (mm):</strong> ${minTol} to ${maxTol}</p>
        <p><strong>Min Buffer (mm):</strong> ${minbuffer}</p>
        <p><strong>Max Buffer (mm):</strong> ${maxbuffer}</p>
    `;
    }
    else if(userData.OTSshaft == true && minbuffer < 100){
        var content = `
        <h2>${searchType} Output</h2>
        <p><strong>Suggested Hole Tolerance:</strong> ${newTol}</p>
        <p><strong>Tolerance Range (mm):</strong> ${minTol} to ${maxTol}</p>
        <p><strong>Min Buffer (mm):</strong> ${minbuffer}</p>
        <p><strong>Max Buffer (mm):</strong> ${maxbuffer}</p>
    `;
    }else{
        var content = `
        <h2>${searchType} Output</h2>
        <p>There was no suggestion found for this range.</p>
        `;
    }

    // Create HTML content to display user data
    

    // Set the content in the display area
    finalContainedOutput.innerHTML = content;
}

function displayTightOutput(minbuffer, maxbuffer, newTol, minTol, maxTol, userData, searchType) {
    // Access the display area in HTML
    var finalTightOutput = document.getElementById('finalTightOutput');

    if(userData.OTShole == true && minbuffer < 100){
        var content = `
        <h2>${searchType} Output</h2>
        <p><strong>Suggested Shaft Tolerance:</strong> ${newTol}</p>
        <p><strong>Tolerance Range (mm):</strong> ${minTol} to ${maxTol}</p>
        <p><strong>Min Buffer (mm):</strong> ${minbuffer}</p>
        <p><strong>Max Buffer (mm):</strong> ${maxbuffer}</p>
    `;
    }
    else if(userData.OTSshaft == true && minbuffer < 100){
        var content = `
        <h2>${searchType} Output</h2>
        <p><strong>Suggested Hole Tolerance:</strong> ${newTol}</p>
        <p><strong>Tolerance Range (mm):</strong> ${minTol} to ${maxTol}</p>
        <p><strong>Min Buffer (mm):</strong> ${minbuffer}</p>
        <p><strong>Max Buffer (mm):</strong> ${maxbuffer}</p>
    `;
    }else{
        var content = `
        <h2>${searchType} Output</h2>
        <p>There was no suggestion found for this range.</p>
        `;
    }

    // Create HTML content to display user data
    

    // Set the content in the display area
    finalTightOutput.innerHTML = content;
}

function displayLooseOutput(minbuffer, maxbuffer, newTol, minTol, maxTol, userData, searchType) {
    // Access the display area in HTML
    var finalLooseOutput = document.getElementById('finalLooseOutput');

    if(userData.OTShole == true && minbuffer < 100){
        var content = `
        <h2>${searchType} Output</h2>
        <p><strong>Suggested Shaft Tolerance:</strong> ${newTol}</p>
        <p><strong>Tolerance Range (mm):</strong> ${minTol} to ${maxTol}</p>
        <p><strong>Min Buffer (mm):</strong> ${minbuffer}</p>
        <p><strong>Max Buffer (mm):</strong> ${maxbuffer}</p>
    `;
    }
    else if(userData.OTSshaft == true && minbuffer < 100){
        var content = `
        <h2>${searchType} Output</h2>
        <p><strong>Suggested Hole Tolerance:</strong> ${newTol}</p>
        <p><strong>Tolerance Range (mm):</strong> ${minTol} to ${maxTol}</p>
        <p><strong>Min Buffer (mm):</strong> ${minbuffer}</p>
        <p><strong>Max Buffer (mm):</strong> ${maxbuffer}</p>
    `;
    }else{
        var content = `
        <h2>${searchType} Output</h2>
        <p>There was no suggestion found for this range.</p>
        `;
    }

    // Create HTML content to display user data
    

    // Set the content in the display area
    finalLooseOutput.innerHTML = content;
}

function checkDFM(prop, myNumber){
    // Regular expression to extract the number from the string
    const numberRegex = /\d+/;
    const numberMatch = prop.match(numberRegex);
        
    // Convert the matched number to an integer
    const number = parseInt(numberMatch[0], 10);

    // Your condition to check if the number is less than yourNumber
    if (number >= myNumber) {
        return true;
    }else{
        return false;
    }
}

function displayUserData(dim, OTSshaft, shaftInput_min, shaftInput_max, OTShole, holeInput_min, holeInput_max, fits, dfm) {
    // Access the display area in HTML
    var displayArea = document.getElementById('displayArea');

    const toleranceObject = tolerances[fits];

    if(OTShole == true){
        var content = `
        <h2>User Data:</h2>
        <p><strong>Nominal Dimension:</strong> ${dim}</p>
        <p><strong>OTS Shaft:</strong> ${OTSshaft ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Shaft Variation (mm):</strong> ${OTSshaft ? shaftInput_min : 'N/A'}</p>
        <p><strong>Maximum Shaft Variation (mm):</strong> ${OTSshaft ? shaftInput_max : 'N/A'}</p>
        <p><strong>OTS Hole:</strong> ${OTShole ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Hole Variation (mm):</strong> ${OTShole ? holeInput_min : 'N/A'}</p>
        <p><strong>Maximum Hole Variation (mm):</strong> ${OTShole ? holeInput_max : 'N/A'}</p>
        <p><strong>Fit:</strong> ${toleranceObject.hb_hole}/${toleranceObject.hb_shaft}</p>
        <p><strong>Minimum DFM Tolerance:</strong> ${dfm}</p>
    `;
    }
    else if(OTSshaft == true){
        var content = `
        <h2>User Data:</h2>
        <p><strong>Nominal Dimension:</strong> ${dim}</p>
        <p><strong>OTS Shaft:</strong> ${OTSshaft ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Shaft Variation (mm):</strong> ${OTSshaft ? shaftInput_min : 'N/A'}</p>
        <p><strong>Maximum Shaft Variation (mm):</strong> ${OTSshaft ? shaftInput_max : 'N/A'}</p>
        <p><strong>OTS Hole:</strong> ${OTShole ? 'Yes' : 'No'}</p>
        <p><strong>Minimum Hole Variation (mm):</strong> ${OTShole ? holeInput_min : 'N/A'}</p>
        <p><strong>Maximum Hole Variation (mm):</strong> ${OTShole ? holeInput_max : 'N/A'}</p>
        <p><strong>Fit:</strong> ${toleranceObject.sb_hole}/${toleranceObject.sb_shaft}</p>
        <p><strong>Minimum DFM Tolerance:</strong> ${dfm}</p>
    `;
    }

    // Create HTML content to display user data
    

    // Set the content in the display area
    displayArea.innerHTML = content;
}

function displayIdealRange(max_search_range, min_search_range) {
    // Access the display area in HTML
    var idealRange = document.getElementById('idealRange');
        var content = `
        <h2>Ideal Range</h2>
        <p><strong>Tolerance Range (mm):</strong> ${min_search_range} to ${max_search_range}</p>
    `;
    // Create HTML content to display user data
    // Set the content in the display area
    idealRange.innerHTML = content;
}

